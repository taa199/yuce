# -*- coding: utf-8 -*-
import os
import csv
import math

root_dir = os.path.dirname(os.path.dirname(__file__))


def get_artist_action_by_day(song_action_dic, action_type, song_artist_dic):
    """
    :param song_artist_dic:
    :param song_action_dic:
    :param action_type:
    :return: 返回一个词典，key是artist_id, 每个value是一个词典，
    词典的key是日期，value是action次数
    """
    artist_action_by_day_dic = {}

    def count_action(list_of_actions, type_):
        count = 0
        for action_info in list_of_actions:
            if action_info[0] == type_:
                count += 1
        return count

    for song_id, all_actions in song_action_dic.items():
        song_actions_count_by_day = {}
        for action_time, actions_at_this_time in all_actions.items():
            song_actions_count_by_day[action_time] = song_actions_count_by_day.get(action_time, 0) + \
                                                     count_action(actions_at_this_time, action_type)
            artist_id = song_artist_dic[song_id]
            if artist_action_by_day_dic.get(artist_id) is None:
                artist_action_by_day_dic[artist_id] = {}
            artist_action_by_day_dic[artist_id][action_time] = \
                artist_action_by_day_dic[artist_id].get(action_time, 0) + song_actions_count_by_day[action_time]

    return artist_action_by_day_dic


def song_avg_to_artist_avg(song_action_avg, song_artist_dic):
    artist_action_avg = {}
    new_song_count = 0
    for song_id, artist_id in song_artist_dic.items():
        try:
            artist_action_avg[artist_id] = artist_action_avg.get(artist_id, 0) + song_action_avg[song_id]
        except KeyError as e:
            print e
            print "this is a new song"
            new_song_count += 1
    print "total new song:%d" % new_song_count
    return artist_action_avg


def get_artist_song_dic():
    filename = os.path.join(root_dir, "raw_data", "mars_tianchi_songs.csv")
    artist_song_dic = {}

    with open(filename, "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            song_id = row[0]
            artist_id = row[1]
            if artist_song_dic.get(artist_id) is None:
                artist_song_dic[artist_id] = []
            artist_song_dic[artist_id].append(song_id)

    return artist_song_dic


def get_song_artist_dic():
    filename = os.path.join(root_dir, "raw_data", "mars_tianchi_songs.csv")
    song_artist_dic = {}

    with open(filename, "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            song_id = row[0]
            artist_id = row[1]

            song_artist_dic[song_id] = artist_id

    return song_artist_dic


def get_song_actions():
    filename = os.path.join(root_dir, "raw_data", "mars_tianchi_user_actions.csv")
    song_action_by_time_dic = {}

    counter = 0
    with open(filename, "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            user_id = row[0]
            song_id = row[1]
            action_time = row[-1]
            action_type = row[3]

            action = (action_type, user_id)

            if song_action_by_time_dic.get(song_id) is None:
                song_action_by_time_dic[song_id] = {}

            if song_action_by_time_dic[song_id].get(action_time) is None:
                song_action_by_time_dic[song_id][action_time] = []

            song_action_by_time_dic[song_id][action_time].append(action)

            counter += 1
            if (counter % 50000) == 0:
                print "processing line %d" % counter

    return song_action_by_time_dic


def get_numeric_data_per_song(song_action_dic, action_type):
    """
    提取数据用来训练
    :param song_action_dic: 这是所有行为
    :param action_type: 这是行为类型
    :return:
    """
    song_action_by_time = {}
    for song_id, all_actions in song_action_dic.items():
        for action_time, actions_at_this_time in all_actions.items():
            song_id_action_time = "|".join([song_id, action_time])
            for (action, user_id) in actions_at_this_time:
                if action == action_type:
                    song_action_by_time[song_id_action_time] = song_action_by_time.get(song_id_action_time, 0) + 1
    return song_action_by_time


def get_average_data_per_song(song_action_dic, action_type):
    """
    提取数据中的平均值
    :param song_action_dic: 这是所有行为
    :param action_type: 这是行为类型
    :return:
    """
    def count_action(list_of_actions, type_):
        count = 0
        for action_info in list_of_actions:
            if action_info[0] == type_:
                count += 1
        return count

    song_action_times = {}
    song_action_days_count = {}
    for song_id, all_actions in song_action_dic.items():
        for action_time, actions_at_this_time in all_actions.items():
            song_action_times[song_id] = song_action_times.get(song_id, 0) + \
                                         count_action(actions_at_this_time, action_type)
            song_action_days_count[song_id] = song_action_days_count.get(song_id, 0) + 1

    song_action_avg = {}
    for k, v in song_action_times.items():
        song_action_avg[k] = int(math.ceil(v / float(song_action_days_count[k])))
    return song_action_avg


def get_play_times_per_song():
    pass

