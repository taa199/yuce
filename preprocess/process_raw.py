# -*- coding: utf-8 -*-
import os
import csv

root_dir = os.path.dirname(os.path.dirname(__file__))


def get_user_song_hotness():
    """
    返回每个用户发生操作的次数，这个词典可以反应用户的活跃程度
    :return:
    """
    filename = os.path.join(root_dir, "raw_data", "mars_tianchi_user_actions.csv")
    user_hotness_dic = {}
    song_hotness_dic = {}
    counter = 0
    with open(filename, "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            user_id = row[0]
            song_id = row[1]
            user_hotness_dic[user_id] = user_hotness_dic.get(user_id, 0) + 1
            song_hotness_dic[song_id] = song_hotness_dic.get(song_id, 0) + 1
            counter += 1
            if (counter % 500000) == 0:
                print "processing line %d" % counter
    return user_hotness_dic, song_hotness_dic


def get_all_artist_song():
    filename = os.path.join(root_dir, "raw_data", "mars_tianchi_songs.csv")
    artist_dic = {}
    song_pub_time_dic = {}
    song_init_play_dic = {}
    counter = 0
    with open(filename, "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            song_id = row[0]
            artist_id = row[1]
            pub_time = row[2]
            play_times = row[3]

            artist_dic[artist_id] = artist_dic.get(artist_id, 0) + 1
            song_pub_time_dic[song_id] = pub_time
            song_init_play_dic[song_id] = play_times
            counter += 1
            if (counter % 5000) == 0:
                print "processing line %d" % counter
    return artist_dic, song_pub_time_dic, song_init_play_dic


def get_all_song():
    filename = os.path.join(root_dir, "raw_data", "mars_tianchi_songs.csv")
    with open(filename, "rb") as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            print "|".join(row)


