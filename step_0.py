# -*- coding: utf-8 -*-
"""
初步分析
"""
from preprocess.process_raw import *


def write_to_file(dic, outfile):
    key_lst = dic.keys()
    with open(outfile, 'wb') as f:
        for k in sorted(key_lst):
            f.write(k)
            f.write("\n")

user_hotness_dic, song_hotness_dic = get_user_song_hotness()
print "用户个数 : %d" % len(user_hotness_dic)
print "歌曲个数 : %d" % len(song_hotness_dic)
write_to_file(song_hotness_dic, "middle_data/action_songs.txt")

artist_dic, song_pub_time_dic, song_init_play_dic = get_all_artist_song()
print "曲库中的艺术家个数 : %d" % len(artist_dic)
print "曲库中的歌曲数: %d" % len(song_pub_time_dic)
write_to_file(song_pub_time_dic, "middle_data/library_songs.txt")

