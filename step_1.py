# -*- coding: utf-8 -*-
"""
提取数据
"""
from preprocess.extract_data import *
import numpy as np
from datetime import date
from utils import daterange
from matplotlib import pyplot


def write_to_file(dic, outfile):
    with open(outfile, "wb") as f:
        for k, v in dic.items():
            song_id, time = k.split("|")
            f.write("{},{},{}\n".format(song_id, time, v))


def write_temp_result(artist_play_avg, outfile):

    with open(outfile, 'wb') as f:
        for k, v in artist_play_avg.items():
            start_date = date(2015, 9, 1)
            end_date = date(2015, 10, 31)
            for single_date in daterange(start_date, end_date):
                f.write("{},{},{}\n".format(k, v, single_date.strftime("%Y-%m-%d")))
"""
saving
"""
# song_action_dic = get_song_actions()
# np.save('middle_data/song_action_dic.npy', song_action_dic)
# print "saved!!"
"""
loading
"""
song_action_dic = np.load('middle_data/song_action_dic.npy').all()
print "loaded!!"

# song_play_times_by_time = get_numeric_data_per_song(song_action_dic, "1")
"""
平均值，包括歌曲每天播放平均值
艺术家每天操作平均值
"""
# song_play_avg = get_average_data_per_song(song_action_dic, "1")
song_artist_dic = get_song_artist_dic()
# artist_song_dic = get_artist_song_dic()
# artist_play_avg = song_avg_to_artist_avg(song_play_avg, song_artist_dic)
# write_temp_result(artist_play_avg, "result/1.csv")
"""
统计每个艺术家每天的操作数
"""
artist_play_by_day = get_artist_action_by_day(song_action_dic, "1", song_artist_dic)
artist_down_by_day = get_artist_action_by_day(song_action_dic, "2", song_artist_dic)
artist_like_by_day = get_artist_action_by_day(song_action_dic, "3", song_artist_dic)

pyplot.clf()
num = 47
print artist_play_by_day.keys()[num]
pyplot.plot(artist_play_by_day[artist_play_by_day.keys()[num]].values(), 'r')
print "play days count : %d" % len(artist_play_by_day[artist_play_by_day.keys()[num]].values())
pyplot.plot(artist_down_by_day[artist_down_by_day.keys()[num]].values(), 'g')
print "down days count : %d" % len(artist_down_by_day[artist_down_by_day.keys()[num]].values())
pyplot.plot(artist_like_by_day[artist_like_by_day.keys()[num]].values(), 'b')
print "like days count : %d" % len(artist_like_by_day[artist_like_by_day.keys()[num]].values())
